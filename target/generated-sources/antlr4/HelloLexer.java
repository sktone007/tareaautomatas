// Generated from Hello.g4 by ANTLR 4.4
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class HelloLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.4", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, INT_TYPE=2, BOOLEAN_TYPE=3, STRING_TYPE=4, REAL_TYPE=5, IF_RW=6, 
		ELSE_RW=7, WHILE_RW=8, FOR_RW=9, AND=10, OR=11, TRUE_LITERAL=12, FALSE_LITERAL=13, 
		ID=14, WS=15;
	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	public static final String[] tokenNames = {
		"'\\u0000'", "'\\u0001'", "'\\u0002'", "'\\u0003'", "'\\u0004'", "'\\u0005'", 
		"'\\u0006'", "'\\u0007'", "'\b'", "'\t'", "'\n'", "'\\u000B'", "'\f'", 
		"'\r'", "'\\u000E'", "'\\u000F'"
	};
	public static final String[] ruleNames = {
		"T__0", "INT_TYPE", "BOOLEAN_TYPE", "STRING_TYPE", "REAL_TYPE", "IF_RW", 
		"ELSE_RW", "WHILE_RW", "FOR_RW", "AND", "OR", "TRUE_LITERAL", "FALSE_LITERAL", 
		"ID", "WS"
	};


	public HelloLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "Hello.g4"; }

	@Override
	public String[] getTokenNames() { return tokenNames; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\2\21\u008f\b\1\4\2"+
		"\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4"+
		"\13\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\3\2\3\2\3\2\3\2"+
		"\3\2\3\2\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\4\3\4\3\4\3\4\3\4\3\4\3"+
		"\4\3\4\3\4\3\4\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\6\3\6\3\6\3\6\3\6\3\6\3\6"+
		"\3\7\3\7\3\7\3\7\3\7\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3"+
		"\b\3\b\3\b\3\b\3\b\3\b\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\n\3\n\3\n"+
		"\3\n\3\n\3\n\3\n\3\13\3\13\3\13\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\r\3\r\3"+
		"\r\3\r\3\r\3\16\3\16\3\16\3\16\3\16\3\17\6\17\u0085\n\17\r\17\16\17\u0086"+
		"\3\20\6\20\u008a\n\20\r\20\16\20\u008b\3\20\3\20\2\2\21\3\3\5\4\7\5\t"+
		"\6\13\7\r\b\17\t\21\n\23\13\25\f\27\r\31\16\33\17\35\20\37\21\3\2\4\3"+
		"\2c|\5\2\13\f\17\17\"\"\u0090\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2\2\t"+
		"\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2\23\3\2\2"+
		"\2\2\25\3\2\2\2\2\27\3\2\2\2\2\31\3\2\2\2\2\33\3\2\2\2\2\35\3\2\2\2\2"+
		"\37\3\2\2\2\3!\3\2\2\2\5\'\3\2\2\2\7\60\3\2\2\2\t:\3\2\2\2\13A\3\2\2\2"+
		"\rH\3\2\2\2\17M\3\2\2\2\21_\3\2\2\2\23h\3\2\2\2\25o\3\2\2\2\27r\3\2\2"+
		"\2\31y\3\2\2\2\33~\3\2\2\2\35\u0084\3\2\2\2\37\u0089\3\2\2\2!\"\7j\2\2"+
		"\"#\7g\2\2#$\7n\2\2$%\7n\2\2%&\7q\2\2&\4\3\2\2\2\'(\7j\2\2()\7w\2\2)*"+
		"\7p\2\2*+\7v\2\2+,\7g\2\2,-\7n\2\2-.\7c\2\2./\7t\2\2/\6\3\2\2\2\60\61"+
		"\7d\2\2\61\62\7q\2\2\62\63\7q\2\2\63\64\7n\2\2\64\65\7k\2\2\65\66\7p\2"+
		"\2\66\67\7i\2\2\678\7g\2\289\7t\2\29\b\3\2\2\2:;\7u\2\2;<\7v\2\2<=\7t"+
		"\2\2=>\7k\2\2>?\7p\2\2?@\7i\2\2@\n\3\2\2\2AB\7o\2\2BC\7c\2\2CD\7f\2\2"+
		"DE\7t\2\2EF\7k\2\2FG\7f\2\2G\f\3\2\2\2HI\7k\2\2IJ\7u\2\2JK\7e\2\2KL\7"+
		"q\2\2L\16\3\2\2\2MN\7g\2\2NO\7n\2\2OP\7u\2\2PQ\7c\2\2QR\7r\2\2RS\7q\2"+
		"\2ST\7N\2\2TU\7k\2\2UV\7x\2\2VW\7k\2\2WX\7p\2\2XY\7i\2\2YZ\7u\2\2Z[\7"+
		"v\2\2[\\\7q\2\2\\]\7p\2\2]^\7g\2\2^\20\3\2\2\2_`\7y\2\2`a\7k\2\2ab\7n"+
		"\2\2bc\7n\2\2cd\7k\2\2de\7c\2\2ef\7o\2\2fg\7u\2\2g\22\3\2\2\2hi\7h\2\2"+
		"ij\7q\2\2jk\7t\2\2kl\7n\2\2lm\7c\2\2mn\7p\2\2n\24\3\2\2\2op\7{\2\2pq\7"+
		"{\2\2q\26\3\2\2\2rs\7q\2\2st\7z\2\2tu\7n\2\2uv\7c\2\2vw\7f\2\2wx\7g\2"+
		"\2x\30\3\2\2\2yz\7i\2\2z{\7q\2\2{|\7c\2\2|}\7n\2\2}\32\3\2\2\2~\177\7"+
		"h\2\2\177\u0080\7q\2\2\u0080\u0081\7w\2\2\u0081\u0082\7n\2\2\u0082\34"+
		"\3\2\2\2\u0083\u0085\t\2\2\2\u0084\u0083\3\2\2\2\u0085\u0086\3\2\2\2\u0086"+
		"\u0084\3\2\2\2\u0086\u0087\3\2\2\2\u0087\36\3\2\2\2\u0088\u008a\t\3\2"+
		"\2\u0089\u0088\3\2\2\2\u008a\u008b\3\2\2\2\u008b\u0089\3\2\2\2\u008b\u008c"+
		"\3\2\2\2\u008c\u008d\3\2\2\2\u008d\u008e\b\20\2\2\u008e \3\2\2\2\5\2\u0086"+
		"\u008b\3\b\2\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}