/**
 * Define a grammar called Hello
 */
grammar Hello;

INT_TYPE: 'huntelar';
BOOLEAN_TYPE: 'boolinger';
STRING_TYPE: 'string';
REAL_TYPE: 'madrid';

IF_RW: 'isco';
ELSE_RW: 'elsapoLivingstone';
WHILE_RW: 'williams';
FOR_RW: 'forlan';

AND: 'yy';
OR: 'oxlade';

TRUE_LITERAL: 'goal';
FALSE_LITERAL: 'foul';



r  : 'hello' ID ;         // match keyword hello followed by an identifier

ID : [a-z]+ ;             // match lower-case identifiers

WS : [ \t\r\n]+ -> skip ; // skip spaces, tabs, newlines

